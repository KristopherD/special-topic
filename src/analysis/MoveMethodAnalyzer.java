/*
 * @(#) ASTAnalyzer.java
 *
 * Copyright 2015-2018 The Software Analysis Laboratory
 * Computer Science, The University of Nebraska at Omaha
 * 6001 Dodge Street, Omaha, NE 68182.
 */
package analysis;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.TypeDeclaration;

import graph.model.GClassNode;
import graph.model.GMethodNode;
import util.UtilMsg;
import util.UtilPlatform;

public class MoveMethodAnalyzer {
   private static final String JAVANATURE = "org.eclipse.jdt.core.javanature";
   private GMethodNode         methodToBeMoved;
   private GClassNode          classMoveDestination;
   private IProject[]          projects;
   private IMethod             iMethod;
   private IType               iType;
   private ICompilationUnit    iCUnitSrc, iCUnitDest;

   private String              pkgSrc     = "pkg1";
   private String              classSrc   = "A";
   private String              methodSrc  = "m1";

   private String              pkgDest    = "pkg1";
   private String              classDest  = "A";
   public MoveMethodAnalyzer() {
	   projects = ResourcesPlugin.getWorkspace().getRoot().getProjects();
   }

   public void analyze() {
	   for (IProject project : projects) {
	         try {
	            analyzeJavaProject(project);
	         } catch (CoreException e) {
	            e.printStackTrace();
	         }
	      }
   }
   private void analyzeCompilationUnit(IPackageFragment iPackage) throws JavaModelException {
	      for (ICompilationUnit iCUnit : iPackage.getCompilationUnits()) {
	         CompilationUnit cUnit = UtilPlatform.parse(iCUnit);

	         ASTVisitor v = new ASTVisitor() {
	            public boolean visit(TypeDeclaration typeDecl) {
	               String pkgName = typeDecl.resolveBinding().getPackage().getName();
	               String className = typeDecl.getName().getFullyQualifiedName();
	               //System.out.println(classMoveDestination.getName());
	               
	               if (className.equals(classMoveDestination.getName())) {
	                  iType = (IType) typeDecl.resolveBinding().getJavaElement();
	                  
	                  iCUnitDest = iCUnit;
	               }
	               return true;
	            }

	            public boolean visit(MethodDeclaration methodDecl) {
	               ITypeBinding curClass = methodDecl.resolveBinding().getDeclaringClass();
	               String pName = curClass.getPackage().getName();
	               String cName = curClass.getName();
	               String mName = methodDecl.getName().getFullyQualifiedName();
	               System.out.println(cName+" "+ mName);
	  	          //   System.out.println(classMoveDestination.getName()+" "+ methodToBeMoved.getName());
	  	          //   System.out.println();
	               if ( //
	               /**/mName.equals(methodToBeMoved.getName())) {
	            	
	                  iMethod = (IMethod) methodDecl.resolveBinding().getJavaElement();
	                  iCUnitSrc = iCUnit;
	               }
	               return true;
	            }
	         };
	         cUnit.accept(v);
	      }
	   }
   private void analyzeJavaProject(IProject project) throws CoreException, JavaModelException {
	      if (!project.isOpen() || !project.isNatureEnabled(JAVANATURE)) {
	         return;
	      }
	      IJavaProject javaProject = JavaCore.create(project);
	      IPackageFragment[] packages = javaProject.getPackageFragments();
	      for (IPackageFragment iPackage : packages) {
	         if (iPackage.getKind() == IPackageFragmentRoot.K_SOURCE) {
	            if (iPackage.getCompilationUnits().length < 1) {
	               continue;
	            }
	            analyzeCompilationUnit(iPackage);
	         }
	      }
	   }
   
   public void setMethodToBeMoved(GMethodNode mNode) {
      this.methodToBeMoved = mNode;
   }

   public void setClassMoveDestination(GClassNode cNode) {
      this.classMoveDestination = cNode;
   }

   public void moveMethod() {
	   System.out.println("-> " + this.iMethod.getElementName());
	     System.out.println("-> " + this.iType.getFullyQualifiedName());
	    if (this.methodToBeMoved.getName().equals(this.classMoveDestination.getName())){
	    	UtilMsg.openWarning("You do not copy \"full method name\" into \"full class name\".");
	    }else{
	      try {
	         iMethod.move(iType, null, null, false, null);
	         UtilPlatform.indentAndSave(iCUnitSrc);
	         UtilPlatform.indentAndSave(iCUnitDest);
	      } catch (JavaModelException e) {
	         e.printStackTrace();
	      }
	    }
   }
}