/*
 * @(#) View.java
 *
 */
package view;

import java.util.ArrayList;
import java.util.EventObject;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.e4.ui.di.Focus;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.events.TypedEvent;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.zest.core.viewers.GraphViewer;
import org.eclipse.zest.core.widgets.Graph;
import org.eclipse.zest.core.widgets.GraphNode;
import org.eclipse.zest.layouts.LayoutStyles;
import org.eclipse.zest.layouts.algorithms.RadialLayoutAlgorithm;
import org.eclipse.zest.layouts.algorithms.TreeLayoutAlgorithm;

import analysis.MoveMethodAnalyzer;
import analysis.NodeDetailAnalyzer;
import analysis.ProjectAnalyzer;
import graph.model.GClassNode;
import graph.model.GConnection;
import graph.model.GMethodNode;
import graph.model.GNode;
import graph.model.GNodeType;
import graph.model.GPackageNode;
import graph.provider.GLabelProvider;
import graph.provider.GModelProvider;
import graph.provider.GNodeContentProvider;
import util.UtilMsg;

public class MyGraphView {
	public static final String VIEW_ID = "simplezestproject5.partdescriptor.simplezestview5";

	private GraphViewer gViewer;
	private int layout = 0;
	private Menu mPopupMenu = null;
	private MenuItem menuItemMoveMethod = null, menuItemRefresh = null, menuItemNodeDetail = null,
			menuItemCompare = null;
	private GraphNode selectedSrcGraphNode = null, selectedDstGraphNode = null;
	private GraphNode prevSelectedDstGraphNode = null;
	private ArrayList<GMethodNode> methodList = new ArrayList<GMethodNode>();
	private GNode selectedMethodNode = null, selectedClassNode = null;
	private GNode prevSelectedClassNode = null;
	ArrayList<GNode> prenodes = new ArrayList<GNode>();
	ArrayList<GConnection> precons = new ArrayList<GConnection>();
	ArrayList<GMethodNode> inMethod = new ArrayList<GMethodNode>();

	@PostConstruct
	public void createControls(Composite parent) {
		gViewer = new GraphViewer(parent, SWT.BORDER);
		gViewer.setContentProvider(new GNodeContentProvider());
		gViewer.setLabelProvider(new GLabelProvider());
		gViewer.setLayoutAlgorithm(new TreeLayoutAlgorithm(LayoutStyles.NO_LAYOUT_NODE_RESIZING), true);
		gViewer.applyLayout();

		addPoupMenu();
		addMouseListenerGraphViewer();
	}

	private void addPoupMenu() {
		mPopupMenu = new Menu(gViewer.getControl());
		gViewer.getControl().setMenu(mPopupMenu);

		menuItemMoveMethod = new MenuItem(mPopupMenu, SWT.CASCADE);
		menuItemMoveMethod.setText("Move Method");
		addSelectionListenerMenuItemMoveMethod();

		menuItemRefresh = new MenuItem(mPopupMenu, SWT.CASCADE);
		menuItemRefresh.setText("Refresh");
		addSelectionListenerMenuItemRefresh();

		menuItemNodeDetail = new MenuItem(mPopupMenu, SWT.CASCADE);
		menuItemNodeDetail.setText("Node Detail");
		addSelectionListenerMenuItemNodeDetail();

		menuItemCompare = new MenuItem(mPopupMenu, SWT.CASCADE);
		menuItemCompare.setText("Compare");
		addSelectionListenerMenuItemCompare();

	}

	private void addMouseListenerGraphViewer() {
		MouseAdapter mouseAdapter = new MouseAdapter() {
			public void mouseDown(MouseEvent e) {
				menuItemMoveMethod.setEnabled(false);
				menuItemNodeDetail.setEnabled(false);
				resetSelectedSrcGraphNode();

				if (isMethodNode(e)) {
					System.out.println("single clicked");
					menuItemMoveMethod.setEnabled(true);
					menuItemNodeDetail.setEnabled(true);

					selectedSrcGraphNode = (GraphNode) ((Graph) e.getSource()).getSelection().get(0);
					selectedSrcGraphNode.setBorderWidth(1);

					selectedMethodNode = (GMethodNode) selectedSrcGraphNode.getData();
					selectedMethodNode.setNodeType(GNodeType.UserSelection);
				}
			}

			@Override
			public void mouseDoubleClick(MouseEvent e) {
				if (isClassNode(e)) {
					System.out.println("double clicked");
					prevSelectedDstGraphNode = selectedDstGraphNode;
					selectedDstGraphNode = (GraphNode) ((Graph) e.getSource()).getSelection().get(0);

					prevSelectedClassNode = selectedClassNode;
					selectedClassNode = (GClassNode) selectedDstGraphNode.getData();

					if (selectedClassNode.eq(prevSelectedClassNode)) {
						// same node => marked => unmarked
						if (selectedClassNode.getNodeType().equals(GNodeType.UserDoubleClicked))
							resetDstNode(selectedDstGraphNode, selectedClassNode);
						// same node => unmarked => marked
						else if (selectedClassNode.getNodeType().equals(GNodeType.InValid))
							changeColorDDClikedNode(selectedClassNode);

					} else {
						// different node => marked && unmarked previous marked
						// node
						changeColorDDClikedNode(selectedClassNode);
						resetDstNode(prevSelectedDstGraphNode, prevSelectedClassNode);
					}
				}
			}
		};
		gViewer.getControl().addMouseListener(mouseAdapter);
	}

	private boolean isClassNode(EventObject e) {
		Object obj = e.getSource();
		Graph graph = (Graph) obj;

		return (!graph.getSelection().isEmpty() && //
				graph.getSelection().get(0) instanceof GraphNode && //
				((GraphNode) graph.getSelection().get(0)).getData() instanceof GClassNode);
	}

	private boolean isMethodNode(EventObject e) {
		Object obj = e.getSource();
		Graph instGraph = (Graph) obj;
		List<?> selection = instGraph.getSelection();
		return ((!selection.isEmpty() && selection.get(0) instanceof GraphNode) && //
				(((GraphNode) selection.get(0)).getData() instanceof GMethodNode));
	}

	private boolean isPackageNode(EventObject e) {
		Object obj = e.getSource();
		Graph graph = (Graph) obj;

		return (!graph.getSelection().isEmpty() && //
				graph.getSelection().get(0) instanceof GraphNode && //
				((GraphNode) graph.getSelection().get(0)).getData() instanceof GPackageNode);
	}

	private void changeColorDDClikedNode(GNode node) {
		if (this.selectedDstGraphNode == null)
			return;
		selectedDstGraphNode.setForegroundColor(ColorConstants.red);
		selectedDstGraphNode.setBackgroundColor(ColorConstants.blue);
		selectedDstGraphNode.setBorderColor(ColorConstants.blue);
		selectedDstGraphNode.setHighlightColor(ColorConstants.blue);
		selectedDstGraphNode.setBorderHighlightColor(ColorConstants.black);
		node.setNodeType(GNodeType.UserDoubleClicked);
	}

	private void resetDstNode(GraphNode graphNode, GNode node) {
		if (graphNode == null || graphNode.isDisposed() || !(node instanceof GPackageNode))
			return;
		graphNode.setForegroundColor(ColorConstants.black);
		graphNode.setBackgroundColor(ColorConstants.green);
		graphNode.setBorderColor(ColorConstants.lightBlue);
		graphNode.setHighlightColor(ColorConstants.lightGreen);
		graphNode.setBorderHighlightColor(ColorConstants.lightBlue);
		node.setNodeType(GNodeType.InValid);
	}

	private void addSelectionListenerMenuItemMoveMethod() {
		SelectionListener menuItemListenerMoveMethod = new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (!isNodesSelected()) {
					String msg = "Please select class and method nodes. " //
							+ "Select a class node by double-click and select a method node by single-click";
					UtilMsg.openWarning(msg);
					System.out.println("[DBG] " + msg);
					return;
				}
				if (((GMethodNode) selectedMethodNode).isParent((GClassNode) selectedClassNode)) {
					String msg = "Please select a different class as move destination.";
					UtilMsg.openWarning(msg);
					System.out.println("[DBG] " + msg);
					return;
				}
				System.out.println("[DBG] MenuItem MoveMethod");
				MoveMethodAnalyzer moveMethodAnalyzer = new MoveMethodAnalyzer();
				moveMethodAnalyzer.setMethodToBeMoved((GMethodNode) selectedMethodNode);
				moveMethodAnalyzer.setClassMoveDestination((GClassNode) selectedClassNode);
				moveMethodAnalyzer.analyze();
				moveMethodAnalyzer.moveMethod();
				resetSelectedSrcGraphNode();
				resetDstNode(selectedDstGraphNode, selectedClassNode);
				syncZestViewAndJavaEditor();
			}

			private boolean isNodesSelected() {
				return selectedMethodNode != null && selectedMethodNode.getNodeType().equals(GNodeType.UserSelection) && //
				selectedClassNode != null && selectedClassNode.getNodeType().equals(GNodeType.UserDoubleClicked);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		};
		menuItemMoveMethod.addSelectionListener(menuItemListenerMoveMethod);
	}

	private void addSelectionListenerMenuItemNodeDetail() {

		SelectionListener menuItemListenerNodeDetail = new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				System.out.println("[DBG] MenuItem NodeDetail");
				GModelProvider.instance().reset();
				new NodeDetailAnalyzer().analyze();
				ArrayList<GNode> temp1 = new ArrayList<GNode>();
				ArrayList<GConnection> temp2 = new ArrayList<GConnection>();
				String selectedMethod = selectedMethodNode.getName();
				ArrayList<GConnection> temp3 = (ArrayList<GConnection>) GModelProvider.instance().getConnections();
				// GModelBuilder.instance().reset();
				System.out.println("Selected method is " + selectedMethodNode.toString());

				for (int p = 0; p < temp3.size(); p++) {
					GConnection element = temp3.get(p);

					if (element.getDestination().getName().equals(selectedMethod)) {
						// System.out.print("Connection: ");
						// System.out.print( element.getSource().getName() + "
						// ");
						// System.out.println(
						// element.getDestination().getName());

						for (int i = 0; i < element.getSource().getConnectedTo().size(); i++) {
							if (!element.getSource().getConnectedTo().get(i).getName().equals(selectedMethod)) {
								// System.out.println("Remove method " +
								// element.getSource().getConnectedTo().get(i).getName());
								element.getSource().getConnectedTo().remove(i);
								i = i - 1;
							}
						}
						String classname = element.getSource().getName();
						System.out.println("Class name is " + classname);
						for (int t = 0; t < temp3.size(); t++) {
							if (temp3.get(t).getDestination().getName().equals(classname)) {
								// System.out.println("the pkg is " +
								// temp3.get(t).getSource().getName());
								for (int n = 0; n < temp3.get(t).getSource().getConnectedTo().size(); n++) {
									if (!temp3.get(t).getSource().getConnectedTo().get(n).getName().equals(classname)) {
										// System.out.println("Remove class " +
										// temp3.get(t).getSource().getConnectedTo().get(n).getName());
										temp3.get(t).getSource().getConnectedTo().remove(n);
										n = n - 1;
									}
								}

								temp1.add(temp3.get(t).getDestination());
								temp1.add(temp3.get(t).getSource());
								temp2.add(temp3.get(t));
							}

						}
						temp1.add(element.getSource());
						temp2.add(element);
						temp1.add(element.getDestination());

					}

				}

				for (int i = 0; i < temp1.size(); i++) {
					// System.out.print(temp1.get(i).getName()+" ");
				}
				System.out.println();
				for (int i = 0; i < temp2.size(); i++) {
					// System.out.println(temp2.get(i).getDestination().getName()+"
					// "+temp2.get(i).getSource().getName());
				}

				GModelProvider.instance().reset();

				GModelProvider.instance().addAll(temp1, temp2);
				for (int i = 0; i < GModelProvider.instance().getNodes().size(); i++) {
					prenodes.add(GModelProvider.instance().getNodes().get(i));
				}
				for (int i = 0; i < GModelProvider.instance().getConnections().size(); i++) {
					precons.add(GModelProvider.instance().getConnections().get(i));

				}
				gViewer.setInput(GModelProvider.instance().getNodes());

				gViewer.refresh();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		};
		menuItemNodeDetail.addSelectionListener(menuItemListenerNodeDetail);
	}

	private void addSelectionListenerMenuItemRefresh() {
		SelectionListener menuItemListenerRefresh = new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				System.out.println("[DBG] MenuItem Refresh");
				methodList.clear();
				inMethod.clear();
				syncZestViewAndJavaEditor();
				GModelProvider.instance().reset();
				new NodeDetailAnalyzer().analyze();
				for (int i = 0; i < GModelProvider.instance().getNodes().size(); i++) {
					if (GModelProvider.instance().getNodes().get(i) instanceof GMethodNode) {
						methodList.add((GMethodNode) GModelProvider.instance().getNodes().get(i));
					}
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		};
		menuItemRefresh.addSelectionListener(menuItemListenerRefresh);
	}

	private void addSelectionListenerMenuItemCompare() {
		SelectionListener menuItemListenerCompare = new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				System.out.println("[DBG] MenuItem Compare");

				int c = 0;
				ArrayList<GNode> curnodes = new ArrayList<GNode>();
				ArrayList<GConnection> curcons = new ArrayList<GConnection>();
				ArrayList<GMethodNode> curMethod = new ArrayList<GMethodNode>();

				GModelProvider.instance().reset();
				new NodeDetailAnalyzer().analyze();
				for (int i = 0; i < GModelProvider.instance().getNodes().size(); i++) {
					curnodes.add(GModelProvider.instance().getNodes().get(i));
				}
				for (int i = 0; i < GModelProvider.instance().getConnections().size(); i++) {
					curcons.add(GModelProvider.instance().getConnections().get(i));
				}
				System.out.println();
				for (int i = 0; i < curnodes.size(); i++) {
					GNode o = curnodes.get(i);
					if (o instanceof GMethodNode) {
						curMethod.add((GMethodNode) o);
					}
				}

				for (int i = 0; i < curMethod.size(); i++) {
					for (int n = 0; n < methodList.size(); n++) {
						if (curMethod.get(i).getName().equals(methodList.get(n).getName())) {

							if (!com(curMethod.get(i).getConnectedTo(), methodList.get(n).getConnectedTo())) {
								if (!inMethod.contains(curMethod.get(i)))
									inMethod.add(curMethod.get(i));
								break;
							}

						}
					}
				}

				for (int i = 0; i < inMethod.size(); i++) {
					for (int n = 0; n < methodList.size(); n++) {
						if (methodList.get(n).getName().equals(inMethod.get(i).getName())) {
							methodList.get(n).counterIn();
							
						}
					}

				}

				for (int i = 0; i < methodList.size(); i++) {
					System.out.println(methodList.get(i).getName());

				}

				for (int n = 0; n < methodList.size(); n++) {
					for (int i = 0; i < GModelProvider.instance().getNodes().size(); i++) {
						if (GModelProvider.instance().getNodes().get(i) instanceof GMethodNode && GModelProvider
								.instance().getNodes().get(i).getName().equals(methodList.get(n).getName())) {
							if (methodList.get(n)
									.getCounter() != ((GMethodNode) GModelProvider.instance().getNodes().get(i))
											.getCounter())
								((GMethodNode) GModelProvider.instance().getNodes().get(i))
										.setCounter(methodList.get(n).getCounter());
						}
					}
				}

				// GModelProvider.instance().reset();
				// GModelProvider.instance().addAll(curnodes, curcons);
				gViewer.setInput(GModelProvider.instance().getNodes());
				gViewer.setLayoutAlgorithm(new TreeLayoutAlgorithm(LayoutStyles.NO_LAYOUT_NODE_RESIZING), true);
				gViewer.refresh();
				// inMethod.clear();
				methodList.clear();
				for (int i = 0; i < GModelProvider.instance().getNodes().size(); i++) {

					if (GModelProvider.instance().getNodes().get(i) instanceof GMethodNode) {
						methodList.add((GMethodNode) GModelProvider.instance().getNodes().get(i));
					}

				}

			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		};
		menuItemCompare.addSelectionListener(menuItemListenerCompare);
	}

	private void resetSelectedSrcGraphNode() {
		if (selectedSrcGraphNode != null && selectedSrcGraphNode.isDisposed() == false) {
			selectedSrcGraphNode.setBorderWidth(0);
			selectedMethodNode.setNodeType(GNodeType.InValid);
		}
	}

	public void syncZestViewAndJavaEditor() {
		new ProjectAnalyzer().analyze();
		gViewer.setInput(GModelProvider.instance().getNodes());
	}

	public void update() {
		gViewer.setInput(GModelProvider.instance().getNodes());
		if (layout % 2 == 0)
			gViewer.setLayoutAlgorithm(new TreeLayoutAlgorithm(LayoutStyles.NO_LAYOUT_NODE_RESIZING), true);
		else
			gViewer.setLayoutAlgorithm(new RadialLayoutAlgorithm(LayoutStyles.NO_LAYOUT_NODE_RESIZING), true);
		layout++;
	}

	public boolean com(List<GNode> list, List<GNode> list2) {
		if (list.size() != list2.size()) {
			System.out.println(list.size());
			System.out.println(list2.size());
			return false;
		} else {

			for (int i = 0; i < list.size(); i++) {
				System.out.println(list.get(i).getName());
				System.out.println(list2.get(i).getName());
				System.out.println();
				if (!list.get(i).getName().equals(list2.get(i).getName())) {
					return false;
				}
			}
		}
		return true;
	}

	@Focus
	public void setFocus() {
		this.gViewer.getGraphControl().setFocus();
	}
}
